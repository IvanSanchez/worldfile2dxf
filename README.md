# worldfile2dxf

Small utility to convert images with a GIS worldfile into a CAD .dxf file



### Usage

* Run `npm install` in order to pull the `image-size` dependency
* Edit `worldfile2dxf.mjs` in order to specify the directory containign the images and worldfiles
* Run `nodejs worldfile2dxf.mjs`


### Legalese

GPLv3. See LICENSE file.
