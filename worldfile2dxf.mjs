
import fs from 'fs';
import path from 'path';
import sizeOf from 'image-size';

// Directory to process
const targetDir = '/home/user/path/to/my/data/';







// const fs = require('fs')
const dir = fs.opendirSync(targetDir)
let dirent;
while ((dirent = dir.readSync()) !== null) {
//   console.log(dirent.name)
	imageToDxf(path.join(targetDir, dirent.name));
}



function imageToDxf(fullFilename) {
	const extension = path.extname(fullFilename);

	let worldFileExtension;
	if (extension === '.jpg' || extension === '.jpeg') {
		worldFileExtension = 'jgw';
	} else if (extension === '.png') {
		worldFileExtension = 'pgw';
	} else if (extension === '.tif' || extension === '.tiff') {
		worldFileExtension = 'tfw';
	} else {
		console.log(`unsupported file extension ${extension}`);
		return;
	}

	const filename = path.basename(fullFilename, extension);
	const dir = path.dirname(fullFilename);
	const worldFileName = path.join(dir, `${filename}.${worldFileExtension}`)
	const {width, height} = sizeOf(fullFilename);

	if (!fs.existsSync(worldFileName)) {
		console.log(`mising worldfile ${worldFileName}`);
		return;
	}

	const worldfile = fs.readFileSync(worldFileName);

	console.log(worldfile.toString());

	const [
		scaleX,
		skewY,
		skewX,
		scaleY,
		topleftX,
		topleftY
	] = worldfile.toString().split("\n").map(Number);

	const bottomleftX = topleftX + skewX * height;
	const bottomleftY = topleftY + scaleY * height;

const dxf = `999
raster2dxf 0.0.0
  0
SECTION
  2
ENTITIES
  0
IMAGE
  5
4F
100
AcDbEntity
  8
0
  6
ByLayer
 62
  256
370
   -1
100
AcDbRasterImage
 10
${bottomleftX}
 20
${bottomleftY}
 30
0
 11
${scaleX}
 21
${skewY}
 31
0
 12
${skewX}
 22
${scaleY}
 32
0
 13
${width}
 23
${height}
340
4D
 70
    1
280
    0
281
   50
282
   50
283
    0
360
4E
  0
ENDSEC
  0
SECTION
  2
OBJECTS
  0
DICTIONARY
  5
C
330
0
100
AcDbDictionary
281
    1
  3
ACAD_GROUP
350
D
  3
ACAD_IMAGE_DICT
350
50
  0
DICTIONARY
  5
D
330
C
100
AcDbDictionary
281
    1
  0
IMAGEDEF_REACTOR
  5
4E
330
4F
100
AcDbRasterImageDefReactor
 90
    2
330
4F
  0
DICTIONARY
  5
50
330
C
100
AcDbDictionary
281
    1
  3
${filename}
350
4D
  0
IMAGEDEF
  5
4D
102
{ACAD_REACTORS
330
4E
102
}
100
AcDbRasterImageDef
 90
    0
  1
${fullFilename}
 10
831
 20
476
 11
1
 21
1
280
    1
281
    0
  0
ENDSEC
  0
EOF
`;

	const dxfFullFilename = path.join(dir, `${filename}.dxf`);
	fs.writeFileSync(dxfFullFilename, dxf);
	console.log(`Written ${dxfFullFilename}`);
};
